# Калькулятор инфляции

[![pipeline status](https://gitlab.com/Serganbus/inflation-calculator/badges/master/pipeline.svg)](https://gitlab.com/Serganbus/inflation-calculator/-/commits/master)
[![coverage report](https://gitlab.com/Serganbus/inflation-calculator/badges/master/coverage.svg)](https://gitlab.com/Serganbus/inflation-calculator/-/commits/master)
[![Latest Release](https://gitlab.com/Serganbus/inflation-calculator/-/badges/release.svg)](https://gitlab.com/Serganbus/inflation-calculator/-/releases)

Содержит интерфейс калькулятора, интерфейс фабрики. Умеет получать инфляцию из сервиса международного валютного фонда(МВФ).

## Установка

Composer: `composer require serganbus/inflation-calculator`

## Использование

Получить данные по инфляции страны, используя данные сервиса МВФ можно так:
```
require __DIR__ . '/vendor/autoload.php';

use Serganbus\Money\Inflation\Adapters\Imf\InflationCalculatorFactory;
use Serganbus\Money\Inflation\InflationCalculatorInterface;

$imfFactory = new InflationCalculatorFactory();

/** @var InflationCalculatorInterface $russianInflationCalculator */
$russianInflationCalculator = $imfFactory->getCountryInflationCalculator('ru');

// Получить инфляцию за январь 2020
$jan2020Inflation = $russianInflationCalculator->getInflationInMonth(2020, 1);

// Получить накопленную инфляцию с 15.06.2020 по 01.01.2021
$from = new \DateTime('2020-06-15');
$to = new \DateTime('2021-01-01');
$inflationBetweenDates = $russianInflationCalculator->getInflationBetweenDates($from, $to);
```

## Расширение
`Serganbus\Money\Inflation\InflationCalculatorInterface` - базовый интерфейс, описывающий методы калькулятора инфляции. `Serganbus\Money\Inflation\InflationCalculator` - реализация интерфейса.
`Serganbus\Money\Inflation\InflationCalculatorFactoryInterface` - Интерфейс фабрики калькуляторов инфляции в зависимости от 2х-символьного ISO-кода страны. `Serganbus\Money\Inflation\Adapters\Imf\InflationCalculatorFactory` - Реализация интерфейса фабрики, получающая данные по инфляции из веб-сервиса Международного Валютного Фонда.

## Источники данных

* [Описание работы API МВФ](https://briandew.files.wordpress.com/2016/03/imf_api-copy13.pdf)
* [Статистика инфляции по России МВФ](http://dataservices.imf.org/REST/SDMX_JSON.svc/CompactData/CPI/M.RU.PCPI_IX?startPeriod=2000&endPeriod=2020)
* [Страны мира, для которых доступна статистика МВФ](http://dataservices.imf.org/REST/SDMX_JSON.svc/CodeList/CL_AREA_CPI)
* [Еще один источник с инфляцией по РФ](https://www.officialdata.org/russia/inflation)
* [И еще один источник с инфляцией по РФ](https://www.global-rates.com/en/information-sources.aspx)
* [И еще один источник с инфляцией по РФ](https://www.statbureau.org/en/russia/cpi)
* [И еще...](https://data.worldbank.org/indicator/NY.GDP.DEFL.KD.ZG.AD?locations=RU&view=csv)
