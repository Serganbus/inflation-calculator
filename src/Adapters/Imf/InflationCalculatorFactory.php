<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation\Adapters\Imf;

use Serganbus\Money\Inflation\InflationCalculatorFactoryInterface;
use Serganbus\Money\Inflation\InflationCalculatorInterface;
use Serganbus\Money\Inflation\InflationCalculator;
use Serganbus\Money\Inflation\CalculatorNotFoundException;
use Psr\SimpleCache\CacheInterface;
use GuzzleHttp\Client as HttpClient;

/**
 * Интерфейс фабрики создания калькулятора инфляции для конкретной страны.
 * Источник данных - Международный Валютный Фонд.
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class InflationCalculatorFactory implements InflationCalculatorFactoryInterface
{
    private HttpClient $httpClient;

    private ?CacheInterface $cache = null;

    public function __construct(?HttpClient $httpClient = null)
    {
        if (is_null($httpClient)) {
            $httpClient = new HttpClient();
        }
        $this->httpClient = $httpClient;
    }

    /** @inheritdoc */
    public function getCountryInflationCalculator(string $countryCode): InflationCalculatorInterface
    {
        return $this->getInflationCalculatorByCountryCode($countryCode);
    }

    /** @inheritdoc */
    public function isCalculatorExist(string $countryCode): bool
    {
        try {
            $this->getInflationCalculatorByCountryCode($countryCode);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /** @inheritdoc */
    public function setCacheStore(CacheInterface $cache): void
    {
        $this->cache = $cache;
    }

    /** @inheritdoc */
    public function getCacheStore(): ?CacheInterface
    {
        return $this->cache;
    }

    /**
     * @throws CalculatorNotFoundException
     */
    private function getInflationCalculatorByCountryCode(string $countryCode): InflationCalculatorInterface
    {
        $countryCode = strtoupper($countryCode);

        if (!is_null($this->cache) && $this->cache->has($countryCode)) {
            /** @var InflationCalculatorInterface $calculator */
            $calculator = $this->cache->get($countryCode);

            return $calculator;
        }

        $inflationMap = $this->getInflationMap($countryCode);

        $calculator = new InflationCalculator($inflationMap);

        if (!is_null($this->cache)) {
            $this->cache->set($countryCode, $calculator);
        }

        return $calculator;
    }

    /**
     * @return array<int, array<int|string, number>>
     */
    private function getInflationMap(string $countryCode): array
    {
        $json = $this->getDataFromService($countryCode);

        // Простой алгоритм, расчитывающий на то,
        // что инфляция по месяцам от веб-сервиса
        // придет в хронологическом порядке
        $inflationMap = [];
        $arrayData = $json->CompactData->DataSet->Series->Obs;
        $prevObsValue = null;
        foreach ($arrayData as $monthData) {
            if (!isset($monthData->{"@OBS_VALUE"})) {
                continue;
            }

            $timePeriod = $monthData->{"@TIME_PERIOD"};
            $obsValue = (float)$monthData->{"@OBS_VALUE"};

            if (!is_null($prevObsValue)) {
                $inflationInMonth = $obsValue * 100 / $prevObsValue - 100;
                [$year, $month] = explode('-', $timePeriod);
                $year = (int)$year;
                $month = (int)$month;

                if (!isset($inflationMap[$year])) {
                    $inflationMap[$year] = [];
                }
                $inflationMap[$year][$month] = $inflationInMonth;
            }

            $prevObsValue = $obsValue;
        }

        return $inflationMap;
    }

    /**
     * Получаем данные по инфляции с веб-сервиса МВФ.
     * Веб-сервис работает нестабильно, поэтому максимальное количество
     * последовательных запросов установлено в 3 штуки.
     *
     * @throws CalculatorNotFoundException
     * @return \stdClass
     */
    private function getDataFromService(string $countryCode): \stdClass
    {
        for ($i = 0; $i < 3; $i++) {
            try {
                $year = date('Y');
                $url = "http://dataservices.imf.org/REST/SDMX_JSON.svc/CompactData/CPI/M.{$countryCode}.PCPI_IX"
                    . "?startPeriod=1900&endPeriod={$year}";

                $response = $this->httpClient->get($url);
                $responseContent = $response->getBody()->getContents();
                $decoded = json_decode($responseContent, false, 512, \JSON_THROW_ON_ERROR);
                if (!($decoded instanceof \stdClass)) {
                    throw new \RuntimeException('Cannot decode json-content');
                }

                return $decoded;
            } catch (\Exception $ex) {
                sleep(1);
            }
        }

        throw new CalculatorNotFoundException("Недоступен веб-сервис imf.");
    }
}
