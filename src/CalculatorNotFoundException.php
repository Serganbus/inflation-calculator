<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation;

/**
 * Дефолтная ошибка получения калькулятора
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class CalculatorNotFoundException extends \Exception
{
    //
}
