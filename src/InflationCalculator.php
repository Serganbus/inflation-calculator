<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation;

/**
 * Реализация интерфейса калькулятора инфляции на основе данных массива.
 * Значение инфляции в массиве в процентах. Данные по инфляции возвращаются также в процентах
 * Пример массива: [
 *   2020 => [
 *     1 => 0.5, 2 => 1, 3 => -3, ..., 12 => 0.4, 'total' => 0.7
 *   ],
 *   2021 => [
 *     ...
 *   ],
 * ]
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class InflationCalculator implements InflationCalculatorInterface
{
    public const DEFAULT_YEARLY_INFLATION = 5;

    /** @var array<int, array<int|string, number>> Карта с инфляционными показателями */
    private array $inflationMap = [];

    private float $defaultYearlyInflation;

    /**
     * @param array<int, array<int|string, number>> $map
     * @param float|null $defaultYearlyInflation
     */
    public function __construct(array $map, float $defaultYearlyInflation = null)
    {
        $this->inflationMap = $map;
        $this->defaultYearlyInflation = $defaultYearlyInflation ?? static::DEFAULT_YEARLY_INFLATION;
    }

    /** @inheritdoc */
    public function getInflationMap(): array
    {
        return $this->inflationMap;
    }

    /** @inheritdoc */
    public function getInflationBetweenDates(\DateTimeInterface $from, \DateTimeInterface $to): float
    {
        $minDate = $from;
        $diff = $from->diff($to);
        if ($diff->invert) {
            $minDate = $to;
        }
        $inflation = 1;
        $currentDate = \DateTime::createFromFormat(\DateTime::W3C, $minDate->format(\DateTime::W3C));
        if (!$currentDate) {
            throw new \RuntimeException('Не удалось сконвертировать DateTimeInterface в DateTime!');
        }
        $currentMonthNum = $currentDate->format('n');
        $currentYearNum = $currentDate->format('Y');
        $daysLeftInCurrentMonth = 0;
        $interval = new \DateInterval('P1D');
        for ($i = 1; $i <= $diff->days; $i++) {
            $daysLeftInCurrentMonth++;
            $daysInCurrentMonth = (int)$currentDate->format('t');
            $currentMonthDayNum = (int)$currentDate->format('j');

            if (
                $daysInCurrentMonth === $currentMonthDayNum
                || $i === $diff->days
            ) {
                $currentMonthNum = (int)$currentDate->format('n');
                $currentYearNum = (int)$currentDate->format('Y');

                $inflationInMonth = $this->getInflationInMonth($currentYearNum, $currentMonthNum) / 100;
                $inflation *= (1 + $inflationInMonth * $daysLeftInCurrentMonth / $daysInCurrentMonth);

                $daysLeftInCurrentMonth = 0;
            }

            $currentDate->add($interval);
        }

        $inflation = $inflation - 1;
        if ($diff->invert) {
            $inflation *= -1;
        }

        return (float)($inflation * 100);
    }

    /** @inheritdoc */
    public function getInflationInMonth(int $year, int $month): float
    {
        if (isset($this->inflationMap[$year][$month])) {
            return (float)$this->inflationMap[$year][$month];
        }

        return $this->defaultYearlyInflation / 12;
    }

    /** @inheritdoc */
    public function getInflationInYear(int $year): float
    {
        if (isset($this->inflationMap[$year]['total'])) {
            return (float)$this->inflationMap[$year]['total'];
        }

        $calculatedMonthlyInflation = 1;
        $canCalcYearlyInflation = true;
        if (isset($this->inflationMap[$year])) {
            for ($month = 1; $month <= 12; $month++) {
                if (!isset($this->inflationMap[$year][$month])) {
                    $canCalcYearlyInflation = false;
                    break;
                }
                $inflationInMonth = $this->inflationMap[$year][$month];

                $calculatedMonthlyInflation *= (1 + $inflationInMonth / 100);
            }
        } else {
            $canCalcYearlyInflation = false;
        }

        if ($canCalcYearlyInflation) {
            $calculatedMonthlyInflation = ($calculatedMonthlyInflation - 1) * 100;

            return $calculatedMonthlyInflation;
        }

        return $this->defaultYearlyInflation;
    }
}
