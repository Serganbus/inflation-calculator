<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation;

use Psr\SimpleCache\CacheInterface;

/**
 * Интерфейс фабрики создания калькулятора инфляции для конкретной страны
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
interface InflationCalculatorFactoryInterface
{
    /**
     * Возвращает true, если есть данные для создания
     * калькулятора инфляции по 2х-символьному коду страны
     * в соответствии с iso-3166.
     *
     * @param string $countryCode
     * @return bool
     */
    public function isCalculatorExist(string $countryCode): bool;

    /**
     * Возвращает калькулятор инфляции для конкретной страны.
     * На вход принимается 2х-символьный код страны в соответствии с iso-3166.
     *
     * @param string $countryCode
     * @return InflationCalculatorInterface
     * @throws CalculatorNotFoundException
     */
    public function getCountryInflationCalculator(string $countryCode): InflationCalculatorInterface;

    /**
     * Устанавливается сущность кеширующего класса
     *
     * @param CacheInterface $cache
     * @return void
     */
    public function setCacheStore(CacheInterface $cache): void;

    /**
     * Возвращается сущность кеширующего класса, если таковой задан.
     * Если кеширующий класс не задан - возвращается null.
     *
     * @return CacheInterface|null
     */
    public function getCacheStore(): ?CacheInterface;
}
