<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation;

/**
 * Позволяет получать параметры инфляции для конкретного месяца и для конкретного года
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
interface InflationCalculatorInterface
{
    /**
     * Запрос инфляции для конкретного месяца года
     *
     * @param int $year Год, для которого запрашивается инфляция
     * @param int $month Месяц, для которого запрашивается инфляция
     * @return float
     */
    public function getInflationInMonth(int $year, int $month): float;

    /**
     * Запрос инфляции для конкретного года
     *
     * @param int $year Год, для которого запрашивается инфляция
     * @return float
     */
    public function getInflationInYear(int $year): float;

    /**
     * Запрос инфляции в промежутке дат
     *
     * @param \DateTimeInterface $from Дата, с которой начинать отсчет
     * @param \DateTimeInterface $to Дата, по которую считать
     * @return float
     */
    public function getInflationBetweenDates(\DateTimeInterface $from, \DateTimeInterface $to): float;

    /**
     * Вернуть все данные по инфляции
     * Пример массива: [
     *   2020 => [
     *     1 => 0.5, 2 => 1, 3 => -3, ..., 12 => 0.4, 'total' => 0.7
     *   ],
     *   2021 => [
     *     ...
     *   ],
     * ]
     *
     * @return array<int, array<int|string, number>>
     */
    public function getInflationMap(): array;
}
