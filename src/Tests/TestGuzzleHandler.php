<?php

declare(strict_types=1);

namespace Serganbus\Money\Inflation\Tests;

use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Response;

/**
 * Фейковый обработчик запросов guzzle.
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class TestGuzzleHandler
{
    private string $rootNs;

    public function __construct(string $rootNs)
    {
        $this->rootNs = $rootNs;
    }

    public function __invoke(RequestInterface $req): FulfilledPromise
    {
        try {
            preg_match('/M\.([A-Z]{2})\.PCPI_IX/', $req->getUri()->getPath(), $matches);
            $countryCode = $matches[1];

            $responseFileName = $this->rootNs . DIRECTORY_SEPARATOR . "{$countryCode}";
            if (!file_exists($responseFileName) || !is_readable($responseFileName)) {
                throw new \Exception("File '{$responseFileName}' not found");
            }

            $responseContent = file_get_contents($responseFileName);
            if ($responseContent === false) {
                throw new \RuntimeException("Cannot read file. File: {$responseFileName}");
            }

            $response = new Response(200, ['X-Header' => 'stub'], $responseContent);
        } catch (\Throwable $ex) {
            $response = new Response(500, ['X-Header' => 'stub'], $ex->getMessage());
        }
        return new FulfilledPromise($response);
    }
}
