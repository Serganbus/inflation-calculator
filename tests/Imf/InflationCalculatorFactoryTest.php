<?php

namespace Serganbus\Money\Inflation\Adapters\Imf;

use Psr\SimpleCache\CacheInterface;
use PHPUnit\Framework\TestCase;
use Serganbus\Money\Inflation\Tests\TestGuzzleHandler;
use Serganbus\Money\Inflation\InflationCalculatorInterface;
use Serganbus\Money\Inflation\CalculatorNotFoundException;
use GuzzleHttp\HandlerStack;

/**
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class InflationCalculatorFactoryTest extends TestCase
{
    private const CALCULATOR_EXIST = 'RU';
    private const CALCULATOR_NOT_EXIST = 'BR';

    private ?InflationCalculatorFactory $factory;

    public function setUp(): void
    {
        $mock = new TestGuzzleHandler(__DIR__ . '/../data');
        $handler = HandlerStack::create($mock);
        $httpConfig['handler'] = $handler;

        $httpClient = new \GuzzleHttp\Client($httpConfig);
        $this->factory = new InflationCalculatorFactory($httpClient);
    }

    public function testInCalculatorExistTrue()
    {
        $this->assertTrue($this->factory->isCalculatorExist(self::CALCULATOR_EXIST));
    }

    public function testInCalculatorExistFalse()
    {
        $this->assertFalse($this->factory->isCalculatorExist(self::CALCULATOR_NOT_EXIST));
    }

    public function testGetCountryInflationCalculatorOk()
    {
        $calculator = $this->factory->getCountryInflationCalculator(self::CALCULATOR_EXIST);
        $this->assertInstanceOf(InflationCalculatorInterface::class, $calculator);
    }

    public function testGetCountryInflationCalculatorOkWithCache()
    {
        $cache = $this->getFactoryCache();
        $this->factory->setCacheStore($cache);
        //without cache
        $calculator = $this->factory->getCountryInflationCalculator(self::CALCULATOR_EXIST);
        $this->assertInstanceOf(InflationCalculatorInterface::class, $calculator);

        //with cache
        $calculator = $this->factory->getCountryInflationCalculator(self::CALCULATOR_EXIST);
        $this->assertInstanceOf(InflationCalculatorInterface::class, $calculator);
    }

    public function testGetCountryInflationCalculatorException()
    {
        $this->expectException(CalculatorNotFoundException::class);
        $this->factory->getCountryInflationCalculator(self::CALCULATOR_NOT_EXIST);
    }

    public function testSetCacheStore()
    {
        $cache = $this->getFactoryCache();
        $this->factory->setCacheStore($cache);
        $this->assertInstanceOf(CacheInterface::class, $this->factory->getCacheStore());
    }

    public function testGetCacheStore()
    {
        $this->assertNull($this->factory->getCacheStore());

        $cache = $this->getFactoryCache();
        $this->factory->setCacheStore($cache);

        $this->assertInstanceOf(CacheInterface::class, $this->factory->getCacheStore());
    }

    private function getFactoryCache()
    {
        return new \Kodus\Cache\FileCache(__DIR__ . '/../data/cache/', 84600);
    }

    public function tearDown(): void
    {
        $this->factory = null;
    }
}
