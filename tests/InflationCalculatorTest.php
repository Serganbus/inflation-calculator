<?php

namespace Serganbus\Money\Inflation;

use PHPUnit\Framework\TestCase;

/**
 * Тесты на калькулятор инфляции
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class InflationCalculatorTest extends TestCase
{
    private const DEFAULT_YEARLY_INFLATION = 0.6;

    private static $inflationMap = [
        2020 => [
            1 => 0.5, 2 => 0.1, 3 => -0.3, 4 => 0.2,
            5 => 1, 6 => 1.1, 7 => -0.3, 8 => -0.8,
            9 => 0.5, 10 => 0.4, 11 => -0.3, 12 => 0,
        ],
        // 2019й нужен только для проверки годовой инфляции
        2019 => [
            'total' => 4.3,
        ],
    ];

    private ?InflationCalculator $calculator;

    public function setUp(): void
    {
        $this->calculator = new InflationCalculator(self::$inflationMap, self::DEFAULT_YEARLY_INFLATION);
    }

    public function testGetInflationMap()
    {
        $this->assertIsArray($this->calculator->getInflationMap());
    }

    public function getInflationInMonthDataProvider()
    {
        return [
            [2020, 1, self::$inflationMap[2020][1]],
            [2020, 6, self::$inflationMap[2020][6]],
            [2020, 12, self::$inflationMap[2020][12]],
            [2021, 1, self::DEFAULT_YEARLY_INFLATION / 12],
        ];
    }

    /**
     * @dataProvider getInflationInMonthDataProvider
     */
    public function testGetInflationInMonth($year, $month, $expectedInflation)
    {
        $actualInflation = $this->calculator->getInflationInMonth($year, $month);
        $this->assertEqualsWithDelta($expectedInflation, $actualInflation, 0.01);
    }

    public function getInflationInYearDataProvider()
    {
        return [
            [2019, self::$inflationMap[2019]['total']],
            [2020, 2.1],
            [2021, self::DEFAULT_YEARLY_INFLATION],
        ];
    }

    /**
     * @dataProvider getInflationInYearDataProvider
     */
    public function testGetInflationInYear($year, $expectedInflation)
    {
        $actualInflation = $this->calculator->getInflationInYear($year);
        $this->assertEqualsWithDelta($expectedInflation, $actualInflation, 0.01);
    }

    public function getInflationBetweenDatesDataProvider()
    {
        return [
            ['2020-01-01', '2020-02-01', 0.5], // есть инфляция, внутри одного месяца
            ['2020-02-01', '2020-02-15', 0.05], // есть инфляция, внутри одного месяца
            ['2020-01-31', '2020-01-15', -0.25], // есть инфляция, внутри одного месяца
            ['2020-12-01', '2020-12-20', 0], // нет инфляции
            ['2020-12-21', '2020-12-02', 0], // нет инфляции
            ['2021-01-01', '2021-07-01', 0.3], // с дефолтной инфляцией, положительная
            ['2021-06-30', '2020-12-31', -0.3], // с дефолтной инфляцией, отрицительная
            ['2020-10-01', '2021-02-15', 0.17], // несколько месяцев затрагиваются, положительная
            ['2021-02-15', '2020-09-30', -0.19], // несколько месяцев затрагиваются, отрицательная
        ];
    }

    /**
     * @dataProvider getInflationBetweenDatesDataProvider
     */
    public function testGetInflationBetweenDates($fromStr, $toStr, $expectedInflation)
    {
        $from = new \DateTime($fromStr);
        $to = new \DateTime($toStr);

        $actualInflation = $this->calculator->getInflationBetweenDates($from, $to);
        $this->assertEqualsWithDelta($expectedInflation, $actualInflation, 0.01);
    }

    public function tearDown(): void
    {
        $this->calculator = null;
    }
}